package ezergil.onemancrew.questbook.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import ezergil.onemancrew.questbook.R;

public class GameFieldListAdapterActions extends BaseAdapter {
    private String[] strings;
    private LayoutInflater inflater;

    public GameFieldListAdapterActions(Context context, String[] strings) {
        this.strings = strings;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return strings.length;
    }

    @Override
    public Object getItem(int i) {
        return strings[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.game_field_list_view_item, null, true);
            holder = new ViewHolder();
            holder.textView = (CheckedTextView) rowView.findViewById(R.id.text_view);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        holder.textView.setText(strings[position]);
        return rowView;
    }

    private class ViewHolder {
        TextView textView;
    }
}