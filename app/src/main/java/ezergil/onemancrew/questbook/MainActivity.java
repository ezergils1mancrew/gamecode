package ezergil.onemancrew.questbook;

import android.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import ezergil.onemancrew.questbook.Adapters.GameFieldListAdapterActions;
import ezergil.onemancrew.questbook.Adapters.GameFieldListAdapterStory;
import ezergil.onemancrew.questbook.Dialogs.InventoryDialog;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView world_view;
    private ListView lv_story, lv_actions;
    private Button inventory, map, menu, quest_details;
    private List<String[]> actions;
    private List<String[]> action_results;
    private List<String> story_holder;
    private List<String> story;
    private List<Integer> world_pics;
    private int action_counter = 0;
    private int chapter = 0;
    public static final String LOG_TAG = "QuestBook";
    private final int STORY_CONTAINER_SIZE = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();
        getStartingPosition();
        setListeners();
        setAdapters();
    }

    private void findViews() {
        world_view = (ImageView) findViewById(R.id.game_field_world_image);
        lv_story = (ListView) findViewById(R.id.game_field_listview_story);
        lv_actions = (ListView) findViewById(R.id.game_field_listview_actions);
        inventory = (Button) findViewById(R.id.game_field_button_inventory);
        map = (Button) findViewById(R.id.game_field_button_map);
        menu = (Button) findViewById(R.id.game_field_button_menu);
        quest_details = (Button) findViewById(R.id.game_field_button_quest_details);
    }

    private void setAdapters() {
        lv_actions.setAdapter(new GameFieldListAdapterActions(this, actions.get(action_counter)));
        lv_story.setAdapter(new GameFieldListAdapterStory(this, story));
    }

    private void setListeners() {
        lv_actions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                actionHandler(position);
            }
        });
        inventory.setOnClickListener(this);
        map.setOnClickListener(this);
        menu.setOnClickListener(this);
        quest_details.setOnClickListener(this);
    }

    private void actionHandler(int position) {
        String txt = action_results.get(action_counter)[position] + "\n" + story_holder.get(action_counter);
        checkForPickingItems(position);
        action_counter++;
        refreshWorld(txt);
    }

    private void getStartingPosition() {
        StoryHolder storyHolder = new StoryHolder(this, chapter);
        actions = storyHolder.getActions();
        action_results = storyHolder.getAction_results();
        story_holder = storyHolder.getStory_holder();
        story = storyHolder.getStory();
        world_pics = storyHolder.getWorld_pics();
        world_view.setBackgroundResource(world_pics.get(action_counter));
    }

    @Override
    public void onClick(View v) {
        String str = "";
        switch (v.getId()) {
            case R.id.game_field_button_inventory:
                str = "Инвентарь v.1.1";
                DialogFragment dialog = new InventoryDialog();
                dialog.show(getFragmentManager(), "InventoryFragment");
                break;
            case R.id.game_field_button_map:
                str = "Карта в разработке";
                break;
            case R.id.game_field_button_menu:
                str = "Меню в разработке";
                break;
            case R.id.game_field_button_quest_details:
                str = "Детали задания в разработке";
                break;
        }
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    private void refreshWorld(String text) {
        Log.i(LOG_TAG, "Ход: " + action_counter);
        Log.i(LOG_TAG, "Текст:\n" + text);
        story.add(text);
        if (story.size() > STORY_CONTAINER_SIZE) {
            story.remove(0);
        }
        lv_story.setAdapter(new GameFieldListAdapterStory(this, story));
        lv_story.setSelection(story.size() - 1);
        lv_actions.setAdapter(new GameFieldListAdapterActions(this, actions.get(action_counter)));
        world_view.setBackgroundResource(world_pics.get(action_counter));
    }

    private void checkForPickingItems(int selectedAction) {
        switch (action_counter) {
            case 6:
                Tools.animateView(inventory, this, R.anim.shrink);
                break;
        }
    }
}
