package ezergil.onemancrew.questbook;

import android.app.Activity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

/**
 * Created by Ezergil on 09.04.2017.
 */

public class Tools {

    public static void animateView(View v, Activity activity, int animationResId) {
        Animation animation = AnimationUtils.loadAnimation(activity, animationResId);
        v.startAnimation(animation);
    }

}
