package ezergil.onemancrew.questbook.Enums;

public enum ArmorType {
    HELMET,
    ARMOR,
    ARMLET,
    GLOVE,
    PANTS,
    BOOTS
}
