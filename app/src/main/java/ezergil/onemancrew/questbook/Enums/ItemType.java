package ezergil.onemancrew.questbook.Enums;

public enum ItemType {
    Armor,
    Weapon,
    Bag
}