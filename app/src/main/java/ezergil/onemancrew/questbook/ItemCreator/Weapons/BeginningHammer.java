package ezergil.onemancrew.questbook.ItemCreator.Weapons;

import ezergil.onemancrew.questbook.ItemCreator.WeaponFactory;

import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.WeaponStatsInterface.beginningHammerCritChance;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.WeaponStatsInterface.beginningHammerDescription;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.WeaponStatsInterface.beginningHammerDmgMax;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.WeaponStatsInterface.beginningHammerDmgMin;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.WeaponStatsInterface.beginningHammerDmgType;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.WeaponStatsInterface.beginningHammerHands;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.WeaponStatsInterface.beginningHammerName;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.WeaponStatsInterface.beginningHammerPictureResId;

public class BeginningHammer extends WeaponFactory{
    {
        this.setName(beginningHammerName);
        this.setDescription(beginningHammerDescription);
        this.setHands(beginningHammerHands);
        this.setDamageType(beginningHammerDmgType);
        this.setPictureResId(beginningHammerPictureResId);
        this.setDmgMin(beginningHammerDmgMin);
        this.setDmgMax(beginningHammerDmgMax);
        this.setCritChance(beginningHammerCritChance);
    }
}
