package ezergil.onemancrew.questbook.ItemCreator.Interfaces;

import android.content.res.Resources;

import ezergil.onemancrew.questbook.R;

public interface BagStatsInterface {
    //НАЗВАНИЕ И ГДЕ ВЗЯЛ, ЧТОБЫ ПОТОМ РАЗОБРАТЬСЯ МОЖНО БЫЛО
    String Name = Resources.getSystem().getString(R.string.empty_string);
    String Description = Resources.getSystem().getString(R.string.empty_string);
    String BagType = "BACKPACK || BELT";
    int PictureResId = 0;
    int AmountOfCells = 0;
    //БАЗОВЫЙ РЮКЗАК
    String basicBackPackName = Resources.getSystem().getString(R.string.basicBackPackName);
    String basicBackPackDescription = Resources.getSystem().getString(R.string.basicBackPackDescription);
    String basicBackPackBagType = Resources.getSystem().getString(R.string.bagType_Backpack);
    int basicBackPackPictureResId = R.mipmap.ic_launcher_round;
    int basicBackPackAmountOfCells = 6;
    //БАЗОВЫЙ ПОЯС
    String basicBeltName = Resources.getSystem().getString(R.string.basicBeltName);
    String basicBeltDescription = Resources.getSystem().getString(R.string.basicBeltDescription);
    String basicBeltBagType = Resources.getSystem().getString(R.string.bagType_Belt);
    int basicBeltPictureResId = R.mipmap.ic_launcher_round;
    int basicBeltAmountOfCells = 2;
}