package ezergil.onemancrew.questbook.ItemCreator.Armors;

import ezergil.onemancrew.questbook.ItemCreator.ArmorFactory;

import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicPantsArmorType;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicPantsDescription;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicPantsFDef;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicPantsMDef;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicPantsName;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicPantsPictureResId;

public class BasicPants extends ArmorFactory {
    {
        this.setName(basicPantsName);
        this.setDescription(basicPantsDescription);
        this.setArmorType(basicPantsArmorType);
        this.setPictureResId(basicPantsPictureResId);
        this.setMDef(basicPantsMDef);
        this.setFDef(basicPantsFDef);
    }
}
