package ezergil.onemancrew.questbook.ItemCreator;

import ezergil.onemancrew.questbook.Enums.DamageType;
import ezergil.onemancrew.questbook.ItemCreator.Interfaces.WeaponGetSetMethodsInterface;
import ezergil.onemancrew.questbook.Enums.ItemType;

public class WeaponFactory implements WeaponGetSetMethodsInterface {

    private String name = "WEAPON";
    private String description = "DESCRIPTION";
    private String hands = "OneHanded || TwoHanded";
    private DamageType dmgType;
    private int pictureResId = 0;
    private int dmgMin = 0;
    private int dmgMax = 0;
    private int critChance = 0;
    private boolean isStackable = false;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getHands() {
        return hands;
    }

    @Override
    public void setHands(String hands) {
        this.hands = hands;
    }

    @Override
    public boolean isStackable() {
        return isStackable;
    }

    @Override
    public ItemType getItemType() {
        return ItemType.Weapon;
    }

    @Override
    public int getPictureResId() {
        return pictureResId;
    }

    @Override
    public void setPictureResId(int pictureResId) {
        this.pictureResId = pictureResId;
    }

    @Override
    public int getDmgMin() {
        return dmgMin;
    }

    @Override
    public void setDmgMin(int dmgMin) {
        this.dmgMin = dmgMin;
    }

    @Override
    public int getDmgMax() {
        return dmgMax;
    }

    @Override
    public void setDmgMax(int dmgMax) {
        this.dmgMax = dmgMax;
    }

    @Override
    public int getCritChance() {
        return critChance;
    }

    @Override
    public void setCritChance(int critChance) {
        this.critChance = critChance;
    }

    @Override
    public DamageType getDamageType() {
        return dmgType;
    }

    @Override
    public void setDamageType(DamageType dmgType) {
        this.dmgType = dmgType;
    }
}