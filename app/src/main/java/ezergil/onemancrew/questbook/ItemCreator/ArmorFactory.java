package ezergil.onemancrew.questbook.ItemCreator;

import ezergil.onemancrew.questbook.Enums.ArmorType;
import ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorGetSetMethodsInterface;
import ezergil.onemancrew.questbook.Enums.ItemType;

public class ArmorFactory implements ArmorGetSetMethodsInterface {

    private String name = "ARMOR";
    private String description = "DESCRIPTION";
    private int pictureResId = 0;
    private int mDef = 0;
    private int fDef = 0;
    private ArmorType armorType;
    private boolean isStackable = false;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int getPictureResId() {
        return pictureResId;
    }

    @Override
    public void setPictureResId(int pictureResId) {
        this.pictureResId = pictureResId;
    }

    @Override
    public int getMDef() {
        return mDef;
    }

    @Override
    public void setMDef(int mDef) {
        this.mDef = mDef;
    }

    @Override
    public int getFDef() {
        return fDef;
    }

    @Override
    public void setFDef(int fDef) {
        this.fDef = fDef;
    }

    @Override
    public ArmorType getArmorType() {
        return armorType;
    }

    @Override
    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }

    @Override
    public boolean isStackable() {
        return isStackable;
    }

    @Override
    public ItemType getItemType() {
        return ItemType.Armor;
    }
}