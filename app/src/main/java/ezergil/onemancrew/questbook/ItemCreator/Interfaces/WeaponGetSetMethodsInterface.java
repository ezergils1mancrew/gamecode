package ezergil.onemancrew.questbook.ItemCreator.Interfaces;

import ezergil.onemancrew.questbook.Enums.DamageType;
import ezergil.onemancrew.questbook.Enums.ItemType;

public interface WeaponGetSetMethodsInterface {
    String getName();

    void setName(String name);

    String getDescription();

    void setDescription(String description);

    int getPictureResId();

    void setPictureResId(int pictureResId);

    int getDmgMin();

    void setDmgMin(int dmgMin);

    int getDmgMax();

    void setDmgMax(int dmgMax);

    int getCritChance();

    void setCritChance(int critChance);

    DamageType getDamageType();

    void setDamageType(DamageType dmgType);

    String getHands();

    void setHands(String hands);

    boolean isStackable();

    ItemType getItemType();
}
