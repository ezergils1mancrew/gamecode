package ezergil.onemancrew.questbook.ItemCreator.Interfaces;

import android.content.res.Resources;

import ezergil.onemancrew.questbook.Enums.ArmorType;
import ezergil.onemancrew.questbook.R;

public interface ArmorStatsInterface {
    //ПИШИ ТАК ЧТОБЫ МОГ ПОНЯТЬ ОТКУДА ЭТА БРОНЬКА ВЗЯЛАСЬ
    String Name = Resources.getSystem().getString(R.string.empty_string);
    String Description = Resources.getSystem().getString(R.string.empty_string);
    int PictureResId = 0;
    int MDef = 0;
    int FDef = 0;
    ArmorType ArmorType = null;
    //БАЗОВАЯ БРОНЯ
    String basicArmorName = Resources.getSystem().getString(R.string.basicArmorName);
    String basicArmorDescription = Resources.getSystem().getString(R.string.basicArmorDescription);
    int basicArmorPictureResId = R.mipmap.ic_launcher_round;
    int basicArmorMDef = 1;
    int basicArmorFDef = 1;
    ArmorType basicArmorArmorType = ArmorType.ARMOR;
    //БАЗОВЫЙ ШЛЕМ
    String basicHelmetName = Resources.getSystem().getString(R.string.basicHelmetName);
    String basicHelmetDescription = Resources.getSystem().getString(R.string.basicHelmetDescription);
    int basicHelmetPictureResId = R.mipmap.ic_launcher_round;
    int basicHelmetMDef = 0;
    int basicHelmetFDef = 1;
    ArmorType basicHelmetArmorType = ArmorType.HELMET;
    //БАЗОВЫЕ НАРУЧИ
    String basicArmletName = Resources.getSystem().getString(R.string.basicArmletName);
    String basicArmletDescription = Resources.getSystem().getString(R.string.basicArmletDescription);
    int basicArmletPictureResId = R.mipmap.ic_launcher_round;
    int basicArmletMDef = 0;
    int basicArmletFDef = 1;
    ArmorType basicArmletArmorType = ArmorType.ARMLET;
    //БАЗОВЫЕ ПЕРЧИ
    String basicGloveName = Resources.getSystem().getString(R.string.basicGloveName);
    String basicGloveDescription = Resources.getSystem().getString(R.string.basicGloveDescription);
    int basicGlovePictureResId = R.mipmap.ic_launcher_round;
    int basicGloveMDef = 0;
    int basicGloveFDef = 1;
    ArmorType basicGloveArmorType = ArmorType.GLOVE;
    //БАЗОВЫЕ ШТАНЫ (ПОНОЖИ)
    String basicPantsName = Resources.getSystem().getString(R.string.basicPantsName);
    String basicPantsDescription = Resources.getSystem().getString(R.string.basicPantsDescription);
    int basicPantsPictureResId = R.mipmap.ic_launcher_round;
    int basicPantsMDef = 1;
    int basicPantsFDef = 1;
    ArmorType basicPantsArmorType = ArmorType.PANTS;
    //БАЗОВЫЕ БОТИНКИ
    String basicBootsName = Resources.getSystem().getString(R.string.basicBootsName);
    String basicBootsDescription = Resources.getSystem().getString(R.string.basicBootsDescription);
    int basicBootsPictureResId = R.mipmap.ic_launcher_round;
    int basicBootsMDef = 0;
    int basicBootsFDef = 1;
    ArmorType basicBootsArmorType = ArmorType.BOOTS;
}
