package ezergil.onemancrew.questbook.ItemCreator;

import ezergil.onemancrew.questbook.ItemCreator.Interfaces.BagGetSetMethodsInterface;
import ezergil.onemancrew.questbook.Enums.ItemType;

public class BagFactory implements BagGetSetMethodsInterface {

    private String name = "BAG";
    private String description = "DESCRIPTION";
    private String bagType = "BACKPACK || BELT";
    private int pictureResId = 0;
    private int amountOfCells = 0;
    private boolean isStackable = false;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getBagType() {
        return bagType;
    }

    @Override
    public void setBagType(String bagType) {
        this.bagType = bagType;
    }

    @Override
    public int getPictureResId() {
        return pictureResId;
    }

    @Override
    public void setPictureResId(int pictureResId) {
        this.pictureResId = pictureResId;
    }

    @Override
    public int getAmountOfCells() {
        return amountOfCells;
    }

    @Override
    public void setAmountOfCells(int amountOfCells) {
        this.amountOfCells = amountOfCells;
    }

    @Override
    public boolean isStackable() {
        return isStackable;
    }

    @Override
    public ItemType getItemType() {
        return ItemType.Bag;
    }
}