package ezergil.onemancrew.questbook.ItemCreator.Armors;

import ezergil.onemancrew.questbook.ItemCreator.ArmorFactory;

import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicArmorArmorType;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicArmorDescription;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicArmorFDef;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicArmorMDef;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicArmorName;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicArmorPictureResId;

public class BasicArmor extends ArmorFactory {
    {
        this.setName(basicArmorName);
        this.setDescription(basicArmorDescription);
        this.setArmorType(basicArmorArmorType);
        this.setPictureResId(basicArmorPictureResId);
        this.setMDef(basicArmorMDef);
        this.setFDef(basicArmorFDef);
    }
}
