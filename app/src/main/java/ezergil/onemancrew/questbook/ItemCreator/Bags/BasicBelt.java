package ezergil.onemancrew.questbook.ItemCreator.Bags;

import ezergil.onemancrew.questbook.ItemCreator.BagFactory;

import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.BagStatsInterface.basicBeltAmountOfCells;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.BagStatsInterface.basicBeltBagType;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.BagStatsInterface.basicBeltDescription;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.BagStatsInterface.basicBeltName;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.BagStatsInterface.basicBeltPictureResId;

public class BasicBelt extends BagFactory {
    {
        this.setName(basicBeltName);
        this.setDescription(basicBeltDescription);
        this.setBagType(basicBeltBagType);
        this.setPictureResId(basicBeltPictureResId);
        this.setAmountOfCells(basicBeltAmountOfCells);
    }
}
