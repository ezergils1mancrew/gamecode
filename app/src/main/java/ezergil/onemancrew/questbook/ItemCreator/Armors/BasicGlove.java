package ezergil.onemancrew.questbook.ItemCreator.Armors;

import ezergil.onemancrew.questbook.ItemCreator.ArmorFactory;

import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicGloveArmorType;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicGloveDescription;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicGloveFDef;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicGloveMDef;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicGloveName;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicGlovePictureResId;

public class BasicGlove extends ArmorFactory {
    {
        this.setName(basicGloveName);
        this.setDescription(basicGloveDescription);
        this.setArmorType(basicGloveArmorType);
        this.setPictureResId(basicGlovePictureResId);
        this.setMDef(basicGloveMDef);
        this.setFDef(basicGloveFDef);
    }
}
