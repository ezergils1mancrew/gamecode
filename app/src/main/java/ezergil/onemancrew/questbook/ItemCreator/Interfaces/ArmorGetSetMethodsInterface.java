package ezergil.onemancrew.questbook.ItemCreator.Interfaces;

import ezergil.onemancrew.questbook.Enums.ArmorType;
import ezergil.onemancrew.questbook.Enums.ItemType;

public interface ArmorGetSetMethodsInterface {
    String getName();

    void setName(String name);

    String getDescription();

    void setDescription(String description);

    int getPictureResId();

    void setPictureResId(int pictureResId);

    int getMDef();

    void setMDef(int mDef);

    int getFDef();

    void setFDef(int fDef);

    ArmorType getArmorType();

    void setArmorType(ArmorType armorType);

    boolean isStackable();

    ItemType getItemType();
}
