package ezergil.onemancrew.questbook.ItemCreator.Interfaces;

import ezergil.onemancrew.questbook.Enums.ItemType;

/**
 * Created by Ezergil on 13.04.2017.
 */

public interface BagGetSetMethodsInterface {

    String getName();

    void setName(String name);

    String getDescription();

    void setDescription(String description);

    String getBagType();

    void setBagType(String bagType);

    int getPictureResId();

    void setPictureResId(int pictureResId);

    int getAmountOfCells();

    void setAmountOfCells(int amountOfCells);

    boolean isStackable();

    ItemType getItemType();
}