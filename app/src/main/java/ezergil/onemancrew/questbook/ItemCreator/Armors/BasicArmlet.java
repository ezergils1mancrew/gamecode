package ezergil.onemancrew.questbook.ItemCreator.Armors;

import ezergil.onemancrew.questbook.ItemCreator.ArmorFactory;

import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicArmletArmorType;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicArmletDescription;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicArmletFDef;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicArmletMDef;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicArmletName;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicArmletPictureResId;

public class BasicArmlet extends ArmorFactory {
    {
        this.setName(basicArmletName);
        this.setDescription(basicArmletDescription);
        this.setArmorType(basicArmletArmorType);
        this.setPictureResId(basicArmletPictureResId);
        this.setMDef(basicArmletMDef);
        this.setFDef(basicArmletFDef);
    }
}
