package ezergil.onemancrew.questbook.ItemCreator.Interfaces;

import android.content.res.Resources;

import ezergil.onemancrew.questbook.Enums.DamageType;
import ezergil.onemancrew.questbook.R;

public interface WeaponStatsInterface {
    //СТАРТОВЫЙ МОЛОТОК
    String beginningHammerName = Resources.getSystem().getString(R.string.beginningHammerName);
    String beginningHammerDescription = Resources.getSystem().getString(R.string.beginningHammerDescription);
    String beginningHammerHands = Resources.getSystem().getString(R.string.weaponHands_OneHanded);
    DamageType beginningHammerDmgType = DamageType.Physic;
    int beginningHammerPictureResId = R.mipmap.ic_launcher_round;
    int beginningHammerDmgMin = 1;
    int beginningHammerDmgMax = 3;
    int beginningHammerCritChance = 2;
}
