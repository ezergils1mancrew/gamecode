package ezergil.onemancrew.questbook.ItemCreator.Armors;

import ezergil.onemancrew.questbook.ItemCreator.ArmorFactory;

import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicBootsArmorType;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicBootsDescription;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicBootsFDef;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicBootsMDef;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicBootsName;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicBootsPictureResId;

public class BasicBoots extends ArmorFactory {
    {
        this.setName(basicBootsName);
        this.setDescription(basicBootsDescription);
        this.setArmorType(basicBootsArmorType);
        this.setPictureResId(basicBootsPictureResId);
        this.setMDef(basicBootsMDef);
        this.setFDef(basicBootsFDef);
    }
}
