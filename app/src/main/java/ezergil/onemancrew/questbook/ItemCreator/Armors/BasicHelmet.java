package ezergil.onemancrew.questbook.ItemCreator.Armors;

import ezergil.onemancrew.questbook.ItemCreator.ArmorFactory;

import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicHelmetArmorType;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicHelmetDescription;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicHelmetFDef;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicHelmetMDef;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicHelmetName;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.ArmorStatsInterface.basicHelmetPictureResId;

public class BasicHelmet extends ArmorFactory {
    {
        this.setName(basicHelmetName);
        this.setDescription(basicHelmetDescription);
        this.setArmorType(basicHelmetArmorType);
        this.setPictureResId(basicHelmetPictureResId);
        this.setMDef(basicHelmetMDef);
        this.setFDef(basicHelmetFDef);
    }
}
