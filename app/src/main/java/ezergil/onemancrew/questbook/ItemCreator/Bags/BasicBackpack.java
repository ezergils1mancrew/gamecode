package ezergil.onemancrew.questbook.ItemCreator.Bags;

import ezergil.onemancrew.questbook.ItemCreator.BagFactory;

import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.BagStatsInterface.basicBackPackAmountOfCells;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.BagStatsInterface.basicBackPackBagType;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.BagStatsInterface.basicBackPackDescription;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.BagStatsInterface.basicBackPackName;
import static ezergil.onemancrew.questbook.ItemCreator.Interfaces.BagStatsInterface.basicBackPackPictureResId;

public class BasicBackpack extends BagFactory {
    {
        this.setName(basicBackPackName);
        this.setDescription(basicBackPackDescription);
        this.setBagType(basicBackPackBagType);
        this.setPictureResId(basicBackPackPictureResId);
        this.setAmountOfCells(basicBackPackAmountOfCells);
    }
}
