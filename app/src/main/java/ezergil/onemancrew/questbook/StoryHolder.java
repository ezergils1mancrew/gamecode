package ezergil.onemancrew.questbook;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ezergil on 03.04.2017.
 */

public class StoryHolder {

    private List<String[]> actions = new ArrayList<String[]>();
    private List<String[]> action_results = new ArrayList<String[]>();
    private List<String> story_holder = new ArrayList<String>();
    private List<String> story = new ArrayList<String>();
    private List<Integer> world_pics = new ArrayList<Integer>();
    private Context context;

    public List<String> getStory() {
        return story;
    }

    public List<String> getStory_holder() {
        return story_holder;
    }

    public List<String[]> getAction_results() {
        return action_results;
    }

    public List<String[]> getActions() {
        return actions;
    }

    public List<Integer> getWorld_pics() {
        return world_pics;
    }

    public StoryHolder(Context context, int chapter) {
        this.context = context;
        setStory(chapter);
    }

    private void setStory(int chapter) {
        switch (chapter) {
            case 0:
                //ДЕЙСТВИЯ (всегда на 1 больше чем остальное)
                actions.add(context.getResources().getStringArray(R.array.chapter_1_actions_0));
                actions.add(context.getResources().getStringArray(R.array.chapter_1_actions_1));
                actions.add(context.getResources().getStringArray(R.array.chapter_1_actions_2));
                actions.add(context.getResources().getStringArray(R.array.chapter_1_actions_3));
                actions.add(context.getResources().getStringArray(R.array.chapter_1_actions_4));
                actions.add(context.getResources().getStringArray(R.array.chapter_1_actions_5));
                actions.add(context.getResources().getStringArray(R.array.chapter_1_actions_6));
                actions.add(context.getResources().getStringArray(R.array.chapter_1_actions_7));
                actions.add(context.getResources().getStringArray(R.array.chapter_1_actions_8));
                actions.add(context.getResources().getStringArray(R.array.chapter_1_actions_9));
                actions.add(context.getResources().getStringArray(R.array.chapter_1_actions_10));
                actions.add(context.getResources().getStringArray(R.array.chapter_1_actions_11));
                actions.add(context.getResources().getStringArray(R.array.chapter_1_actions_12));
                actions.add(context.getResources().getStringArray(R.array.chapter_1_actions_13));
                actions.add(context.getResources().getStringArray(R.array.chapter_1_actions_14));
                actions.add(context.getResources().getStringArray(R.array.chapter_1_actions_15));
                actions.add(context.getResources().getStringArray(R.array.chapter_1_actions_16));
                //РЕЗУЛЬТАТЫ ДЕЙСТВИЙ
                action_results.add(context.getResources().getStringArray(R.array.chapter_1_actions_0_result));
                action_results.add(context.getResources().getStringArray(R.array.chapter_1_actions_1_result));
                action_results.add(context.getResources().getStringArray(R.array.chapter_1_actions_2_result));
                action_results.add(context.getResources().getStringArray(R.array.chapter_1_actions_3_result));
                action_results.add(context.getResources().getStringArray(R.array.chapter_1_actions_4_result));
                action_results.add(context.getResources().getStringArray(R.array.chapter_1_actions_5_result));
                action_results.add(context.getResources().getStringArray(R.array.chapter_1_actions_6_result));
                action_results.add(context.getResources().getStringArray(R.array.chapter_1_actions_7_result));
                action_results.add(context.getResources().getStringArray(R.array.chapter_1_actions_8_result));
                action_results.add(context.getResources().getStringArray(R.array.chapter_1_actions_9_result));
                action_results.add(context.getResources().getStringArray(R.array.chapter_1_actions_10_result));
                action_results.add(context.getResources().getStringArray(R.array.chapter_1_actions_11_result));
                action_results.add(context.getResources().getStringArray(R.array.chapter_1_actions_12_result));
                action_results.add(context.getResources().getStringArray(R.array.chapter_1_actions_13_result));
                action_results.add(context.getResources().getStringArray(R.array.chapter_1_actions_14_result));
                action_results.add(context.getResources().getStringArray(R.array.chapter_1_actions_15_result));
                //ТЕКСТ ПОСЛЕ РЗУЛЬТАТОВ ДЕЙСТВИЙ
                story_holder.add(context.getResources().getString(R.string.chapter_1_actions_0_result_text));
                story_holder.add(context.getResources().getString(R.string.chapter_1_actions_1_result_text));
                story_holder.add(context.getResources().getString(R.string.chapter_1_actions_2_result_text));
                story_holder.add(context.getResources().getString(R.string.chapter_1_actions_3_result_text));
                story_holder.add(context.getResources().getString(R.string.chapter_1_actions_4_result_text));
                story_holder.add(context.getResources().getString(R.string.chapter_1_actions_5_result_text));
                story_holder.add(context.getResources().getString(R.string.chapter_1_actions_6_result_text));
                story_holder.add(context.getResources().getString(R.string.chapter_1_actions_7_result_text));
                story_holder.add(context.getResources().getString(R.string.chapter_1_actions_8_result_text));
                story_holder.add(context.getResources().getString(R.string.chapter_1_actions_9_result_text));
                story_holder.add(context.getResources().getString(R.string.chapter_1_actions_10_result_text));
                story_holder.add(context.getResources().getString(R.string.chapter_1_actions_11_result_text));
                story_holder.add(context.getResources().getString(R.string.chapter_1_actions_12_result_text));
                story_holder.add(context.getResources().getString(R.string.chapter_1_actions_13_result_text));
                story_holder.add(context.getResources().getString(R.string.chapter_1_actions_14_result_text));
                story_holder.add(context.getResources().getString(R.string.chapter_1_actions_15_result_text));
                //КАРТИНКИ
                world_pics.add(R.drawable.chapter_1_start);//0
                world_pics.add(R.drawable.chapter_1_start);
                world_pics.add(R.drawable.chapter_1_action_1_result);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);
                world_pics.add(R.mipmap.ic_launcher_round);//30

                story.add(context.getResources().getString(R.string.chapter_1_beginning_text));
                break;
        }
    }

}
