package ezergil.onemancrew.questbook.Dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;

import ezergil.onemancrew.questbook.R;

public class InventoryDialog extends DialogFragment {

    private ImageView[] beltItems;
    private ImageView[] backpackItems;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.game_field_inventory, container);
        setViews(view);
        Button close = (Button) view.findViewById(R.id.game_field_dialog_inventory_close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    private void setViews(View view) {
        beltItems = new ImageView[]{
                (ImageView) view.findViewById(R.id.game_field_inventory_belt_item_1),
                (ImageView) view.findViewById(R.id.game_field_inventory_belt_item_2),
                (ImageView) view.findViewById(R.id.game_field_inventory_belt_item_3),
                (ImageView) view.findViewById(R.id.game_field_inventory_belt_item_4),
                (ImageView) view.findViewById(R.id.game_field_inventory_belt_item_5),
                (ImageView) view.findViewById(R.id.game_field_inventory_belt_item_6)};
        backpackItems = new ImageView[]{
                (ImageView) view.findViewById(R.id.game_field_inventory_backpack_item_1),
                (ImageView) view.findViewById(R.id.game_field_inventory_backpack_item_2),
                (ImageView) view.findViewById(R.id.game_field_inventory_backpack_item_3),
                (ImageView) view.findViewById(R.id.game_field_inventory_backpack_item_4),
                (ImageView) view.findViewById(R.id.game_field_inventory_backpack_item_5),
                (ImageView) view.findViewById(R.id.game_field_inventory_backpack_item_6),
                (ImageView) view.findViewById(R.id.game_field_inventory_backpack_item_7),
                (ImageView) view.findViewById(R.id.game_field_inventory_backpack_item_8),
                (ImageView) view.findViewById(R.id.game_field_inventory_backpack_item_9),
                (ImageView) view.findViewById(R.id.game_field_inventory_backpack_item_10),
                (ImageView) view.findViewById(R.id.game_field_inventory_backpack_item_11),
                (ImageView) view.findViewById(R.id.game_field_inventory_backpack_item_12),
                (ImageView) view.findViewById(R.id.game_field_inventory_backpack_item_13),
                (ImageView) view.findViewById(R.id.game_field_inventory_backpack_item_14),
                (ImageView) view.findViewById(R.id.game_field_inventory_backpack_item_15),
                (ImageView) view.findViewById(R.id.game_field_inventory_backpack_item_16)};
    }

}
