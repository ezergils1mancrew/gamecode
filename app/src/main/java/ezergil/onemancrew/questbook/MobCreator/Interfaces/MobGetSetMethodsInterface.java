package ezergil.onemancrew.questbook.MobCreator.Interfaces;

import ezergil.onemancrew.questbook.Enums.DamageType;

public interface MobGetSetMethodsInterface {
    String getName();

    void setName(String name);

    String getDescription();

    void setDescription(String description);

    int getPictureResId();

    void setPictureResId(int pictureResId);

    int getMDef();

    void setMDef(int mDef);

    int getFDef();

    void setFDef(int fDef);

    int getHP();

    void setHP(int hp);

    int getHPCurrent();

    void setHPCurrent(int hpCurrent);

    int getMP();

    void setMP(int mp);

    int getMPCurrent();

    void setMPCurrent(int mpCurrent);

    int getDmgMin();

    void setDmgMin(int dmgMin);

    int getDmgMax();

    void setDmgMax(int dmgMax);

    int getCritChance();

    void setCritChance(int critChance);

    DamageType getDamageType();

    void setDamageType(DamageType dmgType);

    String[] getSkills();

    void setSkills(String[] skills);

    int getExpCost();

    void setExpCost(int expCost);
}
