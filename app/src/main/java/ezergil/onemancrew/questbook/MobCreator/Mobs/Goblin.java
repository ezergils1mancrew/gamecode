package ezergil.onemancrew.questbook.MobCreator.Mobs;

import ezergil.onemancrew.questbook.MobCreator.MobFactory;

import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.goblin_critChance;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.goblin_description;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.goblin_dmgMax;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.goblin_dmgMin;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.goblin_dmgType;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.goblin_expCost;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.goblin_fDef;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.goblin_hp;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.goblin_hpCurrent;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.goblin_mDef;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.goblin_mp;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.goblin_mpCurrent;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.goblin_name;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.goblin_pictureResId;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.goblin_skills;

public class Goblin extends MobFactory {
    {
        this.setName(goblin_name);
        this.setDescription(goblin_description);
        this.setPictureResId(goblin_pictureResId);
        this.setFDef(goblin_fDef);
        this.setMDef(goblin_mDef);
        this.setHP(goblin_hp);
        this.setHPCurrent(goblin_hpCurrent);
        this.setMP(goblin_mp);
        this.setMPCurrent(goblin_mpCurrent);
        this.setDmgMin(goblin_dmgMin);
        this.setDmgMax(goblin_dmgMax);
        this.setCritChance(goblin_critChance);
        this.setDamageType(goblin_dmgType);
        this.setSkills(goblin_skills);
        this.setExpCost(goblin_expCost);
    }
}
