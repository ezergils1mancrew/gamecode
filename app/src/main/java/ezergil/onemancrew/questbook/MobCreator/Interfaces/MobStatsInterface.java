package ezergil.onemancrew.questbook.MobCreator.Interfaces;

import android.content.res.Resources;

import ezergil.onemancrew.questbook.Enums.DamageType;
import ezergil.onemancrew.questbook.R;

public interface MobStatsInterface {
    //ГОБЛИН
    String goblin_name = Resources.getSystem().getString(R.string.goblin_name);
    String goblin_description = Resources.getSystem().getString(R.string.goblin_description);
    int goblin_pictureResId = R.drawable.goblin;
    int goblin_fDef = 20;
    int goblin_mDef = 10;
    int goblin_hp = 50;
    int goblin_hpCurrent = 50;
    int goblin_mp = 5;
    int goblin_mpCurrent = 5;
    int goblin_dmgMin = 2;
    int goblin_dmgMax = 10;
    int goblin_critChance = 2;
    DamageType goblin_dmgType = DamageType.Physic;
    String[] goblin_skills = Resources.getSystem().getStringArray(R.array.goblin_skills);
    int goblin_expCost = 5;
    //ПИКСИ
    String pixie_name = Resources.getSystem().getString(R.string.pixie_name);
    String pixie_description = Resources.getSystem().getString(R.string.pixie_description);
    int pixie_pictureResId = R.drawable.pixie;
    int pixie_fDef = 5;
    int pixie_mDef = 50;
    int pixie_hp = 20;
    int pixie_hpCurrent = 20;
    int pixie_mp = 20;
    int pixie_mpCurrent = 20;
    int pixie_dmgMin = 1;
    int pixie_dmgMax = 7;
    int pixie_critChance = 60;
    DamageType pixie_dmgType = DamageType.Magic;
    String[] pixie_skills = Resources.getSystem().getStringArray(R.array.pixie_skills);
    int pixie_expCost = 2;
    //ОРК
    String orc_name = Resources.getSystem().getString(R.string.orc_name);
    String orc_description = Resources.getSystem().getString(R.string.orc_description);
    int orc_pictureResId = R.drawable.orc;
    int orc_fDef = 50;
    int orc_mDef = 5;
    int orc_hp = 70;
    int orc_hpCurrent = 70;
    int orc_mp = 10;
    int orc_mpCurrent = 10;
    int orc_dmgMin = 10;
    int orc_dmgMax = 22;
    int orc_critChance = 35;
    DamageType orc_dmgType = DamageType.Physic;
    String[] orc_skills = Resources.getSystem().getStringArray(R.array.orc_skills);
    int orc_expCost = 25;
}
