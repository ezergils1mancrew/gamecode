package ezergil.onemancrew.questbook.MobCreator;

import ezergil.onemancrew.questbook.Enums.DamageType;
import ezergil.onemancrew.questbook.MobCreator.Interfaces.MobGetSetMethodsInterface;

public class MobFactory implements MobGetSetMethodsInterface {

    private String name = "MOB";
    private String description = "DESCRIPTION";
    private int pictureResId = 0;
    private int fDef = 0;
    private int mDef = 0;
    private int hp = 0;
    private int hpCurrent = 0;
    private int mp = 0;
    private int mpCurrent = 0;
    private int dmgMin = 0;
    private int dmgMax = 0;
    private int critChance = 0;
    private DamageType dmgType;
    private String[] skills = {};
    private int expCost = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPictureResId() {
        return pictureResId;
    }

    public void setPictureResId(int pictureResId) {
        this.pictureResId = pictureResId;
    }

    public int getMDef() {
        return mDef;
    }

    public void setMDef(int mDef) {
        this.mDef = mDef;
    }

    public int getFDef() {
        return fDef;
    }

    public void setFDef(int fDef) {
        this.fDef = fDef;
    }

    public int getHP() {
        return hp;
    }

    public void setHP(int hp) {
        this.hp = hp;
    }

    public int getHPCurrent() {
        return hpCurrent;
    }

    public void setHPCurrent(int hpCurrent) {
        this.hpCurrent = hpCurrent;
    }

    public int getMP() {
        return mp;
    }

    public void setMP(int mp) {
        this.mp = mp;
    }

    public int getMPCurrent() {
        return mpCurrent;
    }

    public void setMPCurrent(int mpCurrent) {
        this.mpCurrent = mpCurrent;
    }

    public int getDmgMin() {
        return dmgMin;
    }

    public void setDmgMin(int dmgMin) {
        this.dmgMin = dmgMin;
    }

    public int getDmgMax() {
        return dmgMax;
    }

    public void setDmgMax(int dmgMax) {
        this.dmgMax = dmgMax;
    }

    public int getCritChance() {
        return critChance;
    }

    public void setCritChance(int critChance) {
        this.critChance = critChance;
    }

    public DamageType getDamageType() {
        return dmgType;
    }

    public void setDamageType(DamageType dmgType) {
        this.dmgType = dmgType;
    }

    public String[] getSkills() {
        return skills;
    }

    public void setSkills(String[] skills) {
        this.skills = skills;
    }

    public int getExpCost() {
        return expCost;
    }

    public void setExpCost(int expCost) {
        this.expCost = expCost;
    }
}
