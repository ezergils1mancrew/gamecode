package ezergil.onemancrew.questbook.MobCreator.Mobs;

import ezergil.onemancrew.questbook.MobCreator.MobFactory;

import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.orc_critChance;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.orc_description;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.orc_dmgMax;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.orc_dmgMin;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.orc_dmgType;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.orc_expCost;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.orc_fDef;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.orc_hp;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.orc_hpCurrent;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.orc_mDef;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.orc_mp;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.orc_mpCurrent;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.orc_name;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.orc_pictureResId;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.orc_skills;

public class Orc extends MobFactory {
    {
        this.setName(orc_name);
        this.setDescription(orc_description);
        this.setPictureResId(orc_pictureResId);
        this.setFDef(orc_fDef);
        this.setMDef(orc_mDef);
        this.setHP(orc_hp);
        this.setHPCurrent(orc_hpCurrent);
        this.setMP(orc_mp);
        this.setMPCurrent(orc_mpCurrent);
        this.setDmgMin(orc_dmgMin);
        this.setDmgMax(orc_dmgMax);
        this.setCritChance(orc_critChance);
        this.setDamageType(orc_dmgType);
        this.setSkills(orc_skills);
        this.setExpCost(orc_expCost);
    }
}
