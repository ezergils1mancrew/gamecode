package ezergil.onemancrew.questbook.MobCreator.Mobs;

import ezergil.onemancrew.questbook.MobCreator.MobFactory;

import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.pixie_critChance;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.pixie_description;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.pixie_dmgMax;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.pixie_dmgMin;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.pixie_dmgType;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.pixie_expCost;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.pixie_fDef;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.pixie_hp;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.pixie_hpCurrent;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.pixie_mDef;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.pixie_mp;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.pixie_mpCurrent;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.pixie_name;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.pixie_pictureResId;
import static ezergil.onemancrew.questbook.MobCreator.Interfaces.MobStatsInterface.pixie_skills;

public class Pixie extends MobFactory {
    {
        this.setName(pixie_name);
        this.setDescription(pixie_description);
        this.setPictureResId(pixie_pictureResId);
        this.setFDef(pixie_fDef);
        this.setMDef(pixie_mDef);
        this.setHP(pixie_hp);
        this.setHPCurrent(pixie_hpCurrent);
        this.setMP(pixie_mp);
        this.setMPCurrent(pixie_mpCurrent);
        this.setDmgMin(pixie_dmgMin);
        this.setDmgMax(pixie_dmgMax);
        this.setCritChance(pixie_critChance);
        this.setDamageType(pixie_dmgType);
        this.setSkills(pixie_skills);
        this.setExpCost(pixie_expCost);
    }
}
